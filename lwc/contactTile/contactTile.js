import { LightningElement, api } from 'lwc';

export default class ContactTile extends LightningElement {
    @api contact;

    get contactId() {
        console.log('--contact Id : ' + this.contact.conId);
        return 'tile' + this.contact.conId;
    }
}