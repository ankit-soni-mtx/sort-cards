import { LightningElement, track } from 'lwc';
import searchContacts from '@salesforce/apex/CardController.searchContacts';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class CardSort extends LightningElement {

    @track contactList;
    @track contactListBackup;
    @track sortDirection = "asc";
    @track sortText = "A-Z";

    connectedCallback() {
        searchContacts({})
            .then((data, error) => {
                if (data) {
                    this.contactList = data;
                    this.pcontactListBackup = data;
                }
                if (error) {
                    const evt = new ShowToastEvent({
                        title: "Error",
                        message: error.message,
                        variant: "error",
                    });
                    this.dispatchEvent(evt);
                }
            });
    }

    doSort() {
        console.log('do Sort');
        const direction = this.sortDirection;
        const fieldName = "name";     // Name of the field to sort for

        let parseData = JSON.parse(JSON.stringify(this.contactList));
        console.log("parseData: ", parseData);
        // Return the value stored in the field
        const keyValue = a => {
            return a[fieldName];
        };
        const getValue = a => {
            return a.value;
        };

        // checking reverse direction
        let isReverse = direction === "asc" ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ""; // handling null values
            y = keyValue(y) ? keyValue(y) : "";

            x = typeof x === "object" ? getValue(x) : x;
            y = typeof y === "object" ? getValue(y) : y;

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        console.log("isReverse ", isReverse);

        //Change to other direction
        if(direction === "asc"){
            this.sortDirection = "desc";
            this.sortText = "Z-A";
        }
        else{
            this.sortDirection = "asc";
            this.sortText = "A-Z";
        }

        console.log("Data Ordered: ", parseData);
        this.contactList = parseData;

    }
}