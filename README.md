## Asset Description :
- This asset contains 1 apex class and 2 lightning web components.
- `cardSort` Lightning Web Component has a `sort` button.
- `contactTile` Lightning Web Component consists the `lightning cards` whicih.
- This apex class can be used to retreive the information to be shown on the card.
- This has been used in several projects where sort functionality is required.

## Asset Use :
- This sort action can be performed on various UI Components - Cards , Lists etc.
- Change the `Field Name` in doSort() javascript method of `cardSort component` as per your requirement.