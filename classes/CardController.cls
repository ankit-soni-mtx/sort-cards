/* 
    CreatedBy : ankitSoni
    Date      : 10/15/2020
*/
public class CardController {
    @AuraEnabled
    public static List<Contacts> searchContacts(){
        List<Contacts> lstContacts = new List<Contacts>();
        
        List<Contact> contactQuery = [
            SELECT Id , Name , Email , Phone , MobilePhone 
            FROM Contact
            LIMIT 6
        ];
        
        for(Contact con : contactQuery){
            Contacts conWrapper = new Contacts();
            conWrapper.conId = con.Id;
            conWrapper.name = con.Name;
            conWrapper.email = con.Email;
            conWrapper.phone = con.Phone;
            conWrapper.mobilePhone = con.MobilePhone;

            lstContacts.add(conWrapper);
        }
    
        return lstContacts;
    }

    public class Contacts{
        @AuraEnabled public String conId;
        @AuraEnabled public String name;
        @AuraEnabled public String email;
        @AuraEnabled public String phone;
        @AuraEnabled public String mobilePhone;        
    }
}
